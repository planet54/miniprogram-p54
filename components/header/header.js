Component({
  mixins: [],
  data: {
    count: 0,
    customer: false
  },
  props: {},
  didMount() {
    const app = getApp();
    const globalData = app.globalData;
    const customer = app.globalData.customer;
    const cart = globalData.cart;
    const count = cart.item_count;
    this.setData({ count, customer });
  },
  didUpdate() {
    const app = getApp();
    const globalData = app.globalData;
    const cart = globalData.cart;
    const count = cart.item_count;
    this.setData({ count });
  },
  didUnmount() {},
  methods: {
    openHome() {
      my.navigateBack({
        delta: 1
      });
    },
    openCollections() {
      my.navigateTo({ url: '../collections/collections' });
    },
    openCart() {
      my.navigateTo({ url: '../cart/cart' });
    },
    openOrders() {
      const app = getApp();
      let customer = app.globalData.customer;
      my.navigateTo({ url: `../orders/orders?id=${customer.shopify.id}` });
    }
  }
});
