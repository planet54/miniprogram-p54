const Shopify = require('../../util/Shopify');

Component({
  mixins: [],
  data: {
    pagination: false,
    toView: 'collectionView',
    scrollTop: 60,
  },
  props: {
    products: (products) => {
      console.log('products =>', products);
    },
    onUpdateProducts: (data) => console.log('onUpdateProducts =>', data),
  },
  didMount() {
    this.pagination();
  },
  didUpdate() {
    this.pagination();
  },
  didUnmount() {},
  methods: {
    onTouch(evt) {
      console.log('touched', evt.target.id);

      if (evt.target.id === 'active-variant') {
        console.log(my.createSelectorQuery().select(`#${evt.target.id}`))
      }
    },
    openMultiLevelSelect() {
      my.multiLevelSelect({
        title: 'Variants ',
        list: [
          {
            name: "Color",//entry name
            subList: [
              {
                name: "Red",
              },
              {
                name: "Green",
              },
              {
                name: "Blue"
              }
            ]// cascade sub-data list
          },
          {
            name: "Size",
            subList: [
              {
                name: "UK 1",
              },
              {
                name: "UK 3",
              },
              {
                name: "UK4"
              }
            ]
          }
        ],// Cascade data list
        success: (res) => {
          my.alert({ title: JSON.stringify(res) })
        }
      });
    },
    pagination() {

      const app = getApp();
      let globalPagination = app.globalData.pagination;

      let pagination = globalPagination;

      this.setData({ pagination });

    },
    onFetchPage(evt) {
      const type = evt.target.dataset.type;
      const page = this.data.pagination[type];

      const promise = Shopify.getPage(page);

      promise.then(res => {
        this.props.onUpdateProducts(res.data.products);
        this.scrollToTop();
      }).catch((err) => {
        console.error('onFetchPage product listings fetch error =>', err);
      });
    },
    scrollToTop() {
      this.setData({
        scrollTop: 0,
      });
    }
  },
});
