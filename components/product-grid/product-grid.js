Component({
  onLoad() { },
  props: {},
  methods: {
    onViewProductPage(evt) {

      if (!evt.target.dataset.id) {
        my.alert('Product ID missing.');
        return;
      }

      my.navigateTo({ url: `../product/product?id=${evt.target.dataset.id}` });

    },
  },
});
