const Cart = require('../../util/cart');

Component({
  mixins: [],
  data: {},
  props: {
    product: (data) => data,
    cart: (data) => data 
  },
  didMount() {},
  didUpdate() {},
  didUnmount() {},
  methods: {
    addToCart() {
      const product = this.props.product;
      const variants = product.variants;
      const productID = product.product_id;

      const multiSelect = [
        {
          name: "Colour",
          subList: [
            {
              name: variants[0].option_values[0].value
            }
          ]
        },
        {
          name: "Size",
          subList: []
        }
      ];

      variants.forEach(variant => {
        if (variant.available) {
          multiSelect[1].subList.push({
            name: variant.option_values[1].value
          });
        }
      });
/*
      const result = [
        {
          name: "Size"
        },
        {
          name: "UK 3"
        }
      ];
*/
      my.multiLevelSelect({
        title: 'Variants ',
        list: multiSelect,
        success: (result) => {
          const selectedItem = variants.filter((variant) => {
            return result[1].name === variant.option_values[1].value;
          });
    
          if (selectedItem.length > 0) {
            Cart.set({
              id: productID,
              variant_id: selectedItem[0].id,
              quantity: 1,
              price: parseInt(selectedItem[0].price.replace('.', ''))
            });

            my.alert('Product Added to Cart');
          }
        },
        fail: (res) => {
          my.alert(JSON.stringify(res))
        }
      });

    }
  },
});
