const storageName = 'Cart';

export default {
  name: storageName,
  get() {
    
    const app = getApp();
    let cart = app.globalData.cart;

    console.log('cart get =>', cart);

    return cart;
  },
  set(item) {

    if (!item) {
      return;
    }

    const app = getApp();
    let cart = app.globalData.cart;

    const existingProduct = cart.items.findIndex((line_item) => line_item.id === item.id);

    cart.total_price = cart.total_price + item.price;
    cart.item_count = cart.item_count + 1;

    if (existingProduct > -1) {
      cart.items[existingProduct].quantity = cart.items[existingProduct].quantity + 1;
    } else {
      cart.items.push(item);
    }

    return cart;
  },
  clear() {
    const app = getApp();
    let cart = app.globalData.cart;

    cart = {
      total_price: 0,
      item_count: 0,
      items: []
    };

    return cart;
  }
};
