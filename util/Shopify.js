const baseURL = 'https://p54-miniprogram.herokuapp.com';
const image = require('./images');

const Shopify = {
  getProductListings: function() {
    return my.request({
      url: `${baseURL}/products?limit=10`,
      method: "GET",
      headers: {},
      dataType: "json",
      success: function (res) {

        const { products, pagination } = res.data;

        products.forEach((product, key) => {
          if (product.images) {
            products[key].thumbnail = image.getSizedImageUrl(product.images[0].src, '190x');
            products[key].image = image.getSizedImageUrl(product.images[0].src, '400x');
          }
        });
        
        const app = getApp();

        app.globalData.pagination = pagination;

        return res;
      },
      fail: function (res) {

        const json = JSON.stringify(res.data);

        my.alert({ content: "product listings failed" + json });

      },

    });
  },
  getProduct: function(id) {
    return my.request({
      url: `${baseURL}/products/${id}`,
      method: 'GET',
      headers: {},
      dataType: "json",
      success: function (res) {
        const { product } = res.data;

        if (!product) return;

        product.thumbnail = image.getSizedImageUrl(product.images[0].src, '190x');
        product.image = image.getSizedImageUrl(product.images[0].src, '400x');
        product.image_small = image.getSizedImageUrl(product.images[0].src, 'small');
  
        return product;
      },
      fail: function () {
        my.alert({ content: 'Error loading product.'});
      },

    });
  },
  getPage: function(page) {

    return my.request({
      url: `${baseURL}/products?page_info=${page}`,
      method: "GET",
      headers: {},
      dataType: "json",
      success: function (res) {

        const { products, pagination } = res.data;

        products.forEach((product, key) => {
          products[key].thumbnail = image.getSizedImageUrl(product.images[0].src, '190x');
          products[key].image = image.getSizedImageUrl(product.images[0].src, '400x');
        });

        const app = getApp();

        app.globalData.pagination = pagination;

        return products;
      },
      fail: function (res) {

        const json = JSON.stringify(res.data);

        my.alert({ content: "product listings failed" + json });

      },

    });
  },
  getCollectionList: function() {
    return my.request({
      url: `${baseURL}/collections`,
      method: 'GET',
      headers: {},
      dataType: "json",
      success: function (res) {
        return  res.data;
      },
      fail: function (res) {
        my.alert({
          title: 'Collection error',
          content: `Cannot fetch menu`
        });
      },

    });
  },
  getProductsFromCollection(id) {
    return my.request({
      url: `${baseURL}/collections/${id}`,
      method: 'GET',
      headers: {},
      dataType: "json",
      success: function (res) {
        const { products, pagination } = res.data;

        products.forEach((product, key) => {
          products[key].thumbnail = image.getSizedImageUrl(product.images[0].src, '190x');
          products[key].image = image.getSizedImageUrl(product.images[0].src, '400x');
        });
        
        const app = getApp();

        app.globalData.pagination = pagination;
    
        return res;
      },
      fail: function () {
        my.alert({
          title: 'Collection error',
          content: `Cannot fetch collection`
        });
      },

    });
  }
}

export default Shopify;
