const Shopify = require('../../util/Shopify');

Page({
  data: {
    showSummery: false,
    line_items: [],
    order: false
  },
  onLoad(params) {
    const self = this;
    let order = this.data.order;
    
    my.request({
      url: `https://p54-miniprogram.herokuapp.com/order/${params.id}`,
      method: 'GET',
      dataType: "json",
      success: function (res) {
        order = res.data

        self.loadProducts(order.line_items);
        self.setData({ order })
      },
      fail: function () { },
    });

  },
  trackOrder(evt) {
    my.navigateTo({ url: `../tracking/tracking?id=${evt.target.targetDataset.value}` })
  },
  loadProducts(products) {
    const self = this;
    let line_items = this.data.line_items;

    products.forEach(product => {

      const productPromise = Shopify.getProduct(product.product_id);

      productPromise.then((res) => {

        const product = res.data.product;

        if (!product) return;

        product.variants.forEach((variant) => {

          if (variant.id === product.variant_id) {
            product.variant = variant;

            let price = parseInt(variant.price.replace('.', ''));

            product.variant.quantity = product.quantity;
            product.variant.price_formatted = self.formatMoney(price);
          }
        });

        line_items.push(product);
        self.setData({ line_items });
      }).catch();
    });

  },
  onShowSummery() {
    let showSummery = this.data.showSummery;

    if (showSummery) {
      showSummery = false;
    } else {
      showSummery = true;
    }

    this.setData({ showSummery });
  }
});
