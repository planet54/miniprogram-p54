const urlBase = 'https://p54-miniprogram.herokuapp.com';

Page({
  data: {
    customerid: false,
    shopify_customer: false,
    setProvinceIndex: 3,
    setCountryIndex: 0,
    formErrors: [],
    provinces: [
      {
        "name": "Eastern Cape",
        "code": "EC"
      },
      {
        "name": "Free State",
        "code": "FS"
      },
      {
        "name": "Gauteng",
        "code": "GT"
      },
      {
        "name": "KwaZulu-Natal",
        "code": "NL"
      },
      {
        "name": "Limpopo",
        "code": "LP"
      },
      {
        "name": "Mpumalanga",
        "code": "MP"
      },
      {
        "name": "North West",
        "code": "NW"
      },
      {
        "name": "Northern Cape",
        "code": "NC"
      },
      {
        "name": "Western Cape",
        "code": "WC"
      }
    ],
    countries: [
      {
        "name": "South Africa",
        "code": "ZA"
      },
    ]
  },
  onLoad(params) {
    const self = this;
    const app = getApp();
    let customer = app.globalData.customer;
    let customerid = this.data.customerid;
    let shopify_customer = this.data.shopify_customer;

    if (params.id) {
      customerid = params.id;

      my.request({
        url: `${urlBase}/user/${customerid}`,
        method: 'GET',
        data: {},
        dataType: "json",
        success: function (res) {
          shopify_customer = res.data;
  
          if (shopify_customer.errors) {
            my.alert({
              title: 'Address Error',
              content: JSON.stringify(data.errors)
            });
            return;
          }

          app.globalData.customer.shopify.addresses.push(shopify_customer)

          self.setData({ shopify_customer });
  
        },
        fail: function (res) {
          const error = res.data.error;
  
          my.alert({
            title: 'Address Error',
            content: JSON.stringify(error)
          });
        },
      });
    }

    this.setData({ customerid });
  },
  formSubmit: function (e) {
    const self = this;
    const app = getApp();
    const formData = e.detail.value;
    const provinces = this.data.provinces;
    const countries = this.data.countries;
    const customerid = this.data.customerid;
    let formErrors = [];

    formData.province = provinces[formData.province].name
    formData.country = countries[formData.country].name

    Object.keys(formData).forEach(key => {
      const field = formData[key];

      if (!field.length) {
        formErrors.push(`• ${key.replace('_', ' ').toUpperCase()} cannot be empty`);
      }
    });

    if (formErrors.length) {
      this.setData({ formErrors });
      return;
    }

    my.request({
      url: `${urlBase}/user/address/update`,
      method: 'POST',
      data: {
        id: customerid,
        address: formData
      },
      dataType: "json",
      success: function (res) {
        const data = res.data;

        if (data.errors) {
          my.alert({
            title: 'Address Error',
            content: JSON.stringify(data.errors)
          });
          return;
        }

        my.navigateTo({ url: `../cart/cart` });

      },
      fail: function (res) {
        const error = res.data.error;

        my.alert({
          title: 'Address Error',
          content: JSON.stringify(error)
        });
      },
    });

    console.log('form has a submit event, carrying data ', formData)
  },
  formReset: function () {
    console.log('form has a reset event')
  },
  bindProvincePickerChange(e) {
    let setProvinceIndex = this.data.setProvinceIndex;
    
    setProvinceIndex = e.detail.value;

    this.setData( { setProvinceIndex });
  },
  bindCountryPickerChange(e) {
    let setCountryIndex = this.data.setCountryIndex;

    setCountryIndex = e.detail.value;

    this.setData( { setCountryIndex });
  },
});
