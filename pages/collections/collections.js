const Shopify = require('../../util/Shopify');

Page({
  data: {
    collections: [],
    products: [],
    viewList: true,
    
  },
  onLoad(params) {
   const collectionsPromise = Shopify.getCollectionList();
   const self = this;
   let viewList = this.data.viewList;

   if (params.id) {
    viewList = false;
    const collectionResponse = Shopify.getProductsFromCollection(params.id);

    collectionResponse.then((res) => {
      const { products } = res.data;
      this.setData({ products });
    });
   } else {
     collectionsPromise.then(function(response) {
       const collections = response.data;
       self.setData({ collections });
     });
   }
   this.setData({ viewList });
  },
  onUpdateProducts(items) {
    const products = items;

    this.setData({ products });
  },
});
