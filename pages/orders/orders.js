const Shopify = require('../../util/Shopify');
const baseURL = 'https://p54-miniprogram.herokuapp.com';
Page({
  data: {
    orders: [],
  },
  onLoad(params) {
    if (params.user_id) {
      this.getOrder(params.user_id)
    }
  },
  getOrder(id) {
    const orders = [
      {
        "id": 3863991058502,
        "admin_graphql_api_id": "gid://shopify/Order/3863991058502",
        "app_id": 1354745,
        "browser_ip": null,
        "buyer_accepts_marketing": false,
        "cancel_reason": null,
        "cancelled_at": null,
        "cart_token": null,
        "checkout_id": 22230909124678,
        "checkout_token": "ecd7a0f6fc023ba2fc26ceff22e33dab",
        "client_details": {
          "accept_language": null,
          "browser_height": null,
          "browser_ip": null,
          "browser_width": null,
          "session_hash": null,
          "user_agent": null
        },
        "closed_at": null,
        "confirmed": true,
        "contact_email": "ian+customer@owira.co.za",
        "created_at": "2021-08-06T15:39:23+02:00",
        "currency": "ZAR",
        "current_subtotal_price": "179.99",
        "current_subtotal_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "current_total_discounts": "0.00",
        "current_total_discounts_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "current_total_duties_set": null,
        "current_total_price": "179.99",
        "current_total_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "current_total_tax": "0.00",
        "current_total_tax_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "customer_locale": "en",
        "device_id": null,
        "discount_codes": [],
        "email": "ian+customer@owira.co.za",
        "financial_status": "paid",
        "fulfillment_status": null,
        "gateway": "manual",
        "landing_site": null,
        "landing_site_ref": null,
        "location_id": null,
        "name": "2000156736",
        "note": null,
        "note_attributes": [],
        "number": 155736,
        "order_number": 156736,
        "order_status_url": "https://planet54.com/19759333/orders/60a22dd6a07105ae2b5db2c19cf6bb59/authenticate?key=f6f6ee8dd6b1c2a6b5a0f46822f18727",
        "original_total_duties_set": null,
        "payment_gateway_names": [
          "manual"
        ],
        "phone": "+27845741159",
        "presentment_currency": "ZAR",
        "processed_at": "2021-08-06T15:39:23+02:00",
        "processing_method": "manual",
        "reference": null,
        "referring_site": null,
        "source_identifier": null,
        "source_name": "shopify_draft_order",
        "source_url": null,
        "subtotal_price": "179.99",
        "subtotal_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "tags": "",
        "tax_lines": [],
        "taxes_included": false,
        "test": false,
        "token": "60a22dd6a07105ae2b5db2c19cf6bb59",
        "total_discounts": "0.00",
        "total_discounts_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "total_line_items_price": "179.99",
        "total_line_items_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "total_outstanding": "0.00",
        "total_price": "179.99",
        "total_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "total_price_usd": "12.40",
        "total_shipping_price_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "total_tax": "0.00",
        "total_tax_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "total_tip_received": "0.00",
        "total_weight": 500,
        "updated_at": "2021-08-06T15:39:27+02:00",
        "user_id": 117562257,
        "billing_address": {
          "first_name": "Ian",
          "address1": "88 riff road",
          "phone": null,
          "city": "New City",
          "zip": "10384",
          "province": "KwaZulu-Natal",
          "country": "South Africa",
          "last_name": "Owira",
          "address2": "Building 44",
          "company": null,
          "latitude": null,
          "longitude": null,
          "name": "Ian Owira",
          "country_code": "ZA",
          "province_code": "NL"
        },
        "customer": {
          "id": 5944958353,
          "email": "ian+customer@owira.co.za",
          "accepts_marketing": false,
          "created_at": "2017-07-31T22:07:26+02:00",
          "updated_at": "2021-08-06T15:39:37+02:00",
          "first_name": "Ian",
          "last_name": "Owira",
          "orders_count": 14,
          "state": "enabled",
          "total_spent": "2189.92",
          "last_order_id": 3863991058502,
          "note": "",
          "verified_email": true,
          "multipass_identifier": null,
          "tax_exempt": false,
          "phone": "+27845741159",
          "tags": "Black Friday signup, blackfriday2019, blackfridaysms1, cybermonday, Female, Galactic, gender_Female, more4, newsletter, paydaysms, Planetary, Super Nova, testsms",
          "last_order_name": "2000156736",
          "currency": "ZAR",
          "accepts_marketing_updated_at": "2020-10-31T12:28:37+02:00",
          "marketing_opt_in_level": null,
          "tax_exemptions": [],
          "admin_graphql_api_id": "gid://shopify/Customer/5944958353",
          "default_address": {
            "id": 6653918150726,
            "customer_id": 5944958353,
            "first_name": "Ian",
            "last_name": "Owira",
            "company": null,
            "address1": "88 riff road",
            "address2": "Building 44",
            "city": "New City",
            "province": "KwaZulu-Natal",
            "country": "South Africa",
            "zip": "10384",
            "phone": null,
            "name": "Ian Owira",
            "province_code": "NL",
            "country_code": "ZA",
            "country_name": "South Africa",
            "default": true
          }
        },
        "discount_applications": [],
        "fulfillments": [],
        "line_items": [
          {
            "id": 10039083630662,
            "admin_graphql_api_id": "gid://shopify/LineItem/10039083630662",
            "destination_location": {
              "id": 2945537638470,
              "country_code": "ZA",
              "province_code": "NL",
              "name": "Ian Owira",
              "address1": "88 riff road",
              "address2": "Building 44",
              "city": "New City",
              "zip": "10384"
            },
            "fulfillable_quantity": 1,
            "fulfillment_service": "manual",
            "fulfillment_status": null,
            "gift_card": false,
            "grams": 500,
            "name": "2 Piece Outfit - Green - Green / 38",
            "origin_location": {
              "id": 1403065991238,
              "country_code": "ZA",
              "province_code": "NL",
              "name": "Planet54",
              "address1": "42 Brickworks Way, Durban North",
              "address2": "",
              "city": "Durban",
              "zip": "4051"
            },
            "pre_tax_price": "179.99",
            "pre_tax_price_set": {
              "shop_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              },
              "presentment_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              }
            },
            "price": "179.99",
            "price_set": {
              "shop_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              },
              "presentment_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              }
            },
            "product_exists": true,
            "product_id": 4601084837958,
            "properties": [],
            "quantity": 1,
            "requires_shipping": true,
            "sku": "100000177121",
            "taxable": true,
            "title": "2 Piece Outfit - Green",
            "total_discount": "0.00",
            "total_discount_set": {
              "shop_money": {
                "amount": "0.00",
                "currency_code": "ZAR"
              },
              "presentment_money": {
                "amount": "0.00",
                "currency_code": "ZAR"
              }
            },
            "variant_id": 31918572306502,
            "variant_inventory_management": "shopify",
            "variant_title": "Green / 38",
            "vendor": "Urban Style",
            "tax_lines": [],
            "duties": [],
            "discount_allocations": []
          }
        ],
        "refunds": [],
        "shipping_address": {
          "first_name": "Ian",
          "address1": "88 riff road",
          "phone": null,
          "city": "New City",
          "zip": "10384",
          "province": "KwaZulu-Natal",
          "country": "South Africa",
          "last_name": "Owira",
          "address2": "Building 44",
          "company": null,
          "latitude": null,
          "longitude": null,
          "name": "Ian Owira",
          "country_code": "ZA",
          "province_code": "NL"
        },
        "shipping_lines": []
      },
      {
        "id": 3863991058502,
        "admin_graphql_api_id": "gid://shopify/Order/3863991058502",
        "app_id": 1354745,
        "browser_ip": null,
        "buyer_accepts_marketing": false,
        "cancel_reason": null,
        "cancelled_at": null,
        "cart_token": null,
        "checkout_id": 22230909124678,
        "checkout_token": "ecd7a0f6fc023ba2fc26ceff22e33dab",
        "client_details": {
          "accept_language": null,
          "browser_height": null,
          "browser_ip": null,
          "browser_width": null,
          "session_hash": null,
          "user_agent": null
        },
        "closed_at": null,
        "confirmed": true,
        "contact_email": "ian+customer@owira.co.za",
        "created_at": "2021-08-06T15:39:23+02:00",
        "currency": "ZAR",
        "current_subtotal_price": "179.99",
        "current_subtotal_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "current_total_discounts": "0.00",
        "current_total_discounts_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "current_total_duties_set": null,
        "current_total_price": "179.99",
        "current_total_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "current_total_tax": "0.00",
        "current_total_tax_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "customer_locale": "en",
        "device_id": null,
        "discount_codes": [],
        "email": "ian+customer@owira.co.za",
        "financial_status": "paid",
        "fulfillment_status": null,
        "gateway": "manual",
        "landing_site": null,
        "landing_site_ref": null,
        "location_id": null,
        "name": "2000156736",
        "note": null,
        "note_attributes": [],
        "number": 155736,
        "order_number": 156736,
        "order_status_url": "https://planet54.com/19759333/orders/60a22dd6a07105ae2b5db2c19cf6bb59/authenticate?key=f6f6ee8dd6b1c2a6b5a0f46822f18727",
        "original_total_duties_set": null,
        "payment_gateway_names": [
          "manual"
        ],
        "phone": "+27845741159",
        "presentment_currency": "ZAR",
        "processed_at": "2021-08-06T15:39:23+02:00",
        "processing_method": "manual",
        "reference": null,
        "referring_site": null,
        "source_identifier": null,
        "source_name": "shopify_draft_order",
        "source_url": null,
        "subtotal_price": "179.99",
        "subtotal_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "tags": "",
        "tax_lines": [],
        "taxes_included": false,
        "test": false,
        "token": "60a22dd6a07105ae2b5db2c19cf6bb59",
        "total_discounts": "0.00",
        "total_discounts_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "total_line_items_price": "179.99",
        "total_line_items_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "total_outstanding": "0.00",
        "total_price": "179.99",
        "total_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "total_price_usd": "12.40",
        "total_shipping_price_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "total_tax": "0.00",
        "total_tax_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "total_tip_received": "0.00",
        "total_weight": 500,
        "updated_at": "2021-08-06T15:39:27+02:00",
        "user_id": 117562257,
        "billing_address": {
          "first_name": "Ian",
          "address1": "88 riff road",
          "phone": null,
          "city": "New City",
          "zip": "10384",
          "province": "KwaZulu-Natal",
          "country": "South Africa",
          "last_name": "Owira",
          "address2": "Building 44",
          "company": null,
          "latitude": null,
          "longitude": null,
          "name": "Ian Owira",
          "country_code": "ZA",
          "province_code": "NL"
        },
        "customer": {
          "id": 5944958353,
          "email": "ian+customer@owira.co.za",
          "accepts_marketing": false,
          "created_at": "2017-07-31T22:07:26+02:00",
          "updated_at": "2021-08-06T15:39:37+02:00",
          "first_name": "Ian",
          "last_name": "Owira",
          "orders_count": 14,
          "state": "enabled",
          "total_spent": "2189.92",
          "last_order_id": 3863991058502,
          "note": "",
          "verified_email": true,
          "multipass_identifier": null,
          "tax_exempt": false,
          "phone": "+27845741159",
          "tags": "Black Friday signup, blackfriday2019, blackfridaysms1, cybermonday, Female, Galactic, gender_Female, more4, newsletter, paydaysms, Planetary, Super Nova, testsms",
          "last_order_name": "2000156736",
          "currency": "ZAR",
          "accepts_marketing_updated_at": "2020-10-31T12:28:37+02:00",
          "marketing_opt_in_level": null,
          "tax_exemptions": [],
          "admin_graphql_api_id": "gid://shopify/Customer/5944958353",
          "default_address": {
            "id": 6653918150726,
            "customer_id": 5944958353,
            "first_name": "Ian",
            "last_name": "Owira",
            "company": null,
            "address1": "88 riff road",
            "address2": "Building 44",
            "city": "New City",
            "province": "KwaZulu-Natal",
            "country": "South Africa",
            "zip": "10384",
            "phone": null,
            "name": "Ian Owira",
            "province_code": "NL",
            "country_code": "ZA",
            "country_name": "South Africa",
            "default": true
          }
        },
        "discount_applications": [],
        "fulfillments": [],
        "line_items": [
          {
            "id": 10039083630662,
            "admin_graphql_api_id": "gid://shopify/LineItem/10039083630662",
            "destination_location": {
              "id": 2945537638470,
              "country_code": "ZA",
              "province_code": "NL",
              "name": "Ian Owira",
              "address1": "88 riff road",
              "address2": "Building 44",
              "city": "New City",
              "zip": "10384"
            },
            "fulfillable_quantity": 1,
            "fulfillment_service": "manual",
            "fulfillment_status": null,
            "gift_card": false,
            "grams": 500,
            "name": "2 Piece Outfit - Green - Green / 38",
            "origin_location": {
              "id": 1403065991238,
              "country_code": "ZA",
              "province_code": "NL",
              "name": "Planet54",
              "address1": "42 Brickworks Way, Durban North",
              "address2": "",
              "city": "Durban",
              "zip": "4051"
            },
            "pre_tax_price": "179.99",
            "pre_tax_price_set": {
              "shop_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              },
              "presentment_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              }
            },
            "price": "179.99",
            "price_set": {
              "shop_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              },
              "presentment_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              }
            },
            "product_exists": true,
            "product_id": 4601084837958,
            "properties": [],
            "quantity": 1,
            "requires_shipping": true,
            "sku": "100000177121",
            "taxable": true,
            "title": "2 Piece Outfit - Green",
            "total_discount": "0.00",
            "total_discount_set": {
              "shop_money": {
                "amount": "0.00",
                "currency_code": "ZAR"
              },
              "presentment_money": {
                "amount": "0.00",
                "currency_code": "ZAR"
              }
            },
            "variant_id": 31918572306502,
            "variant_inventory_management": "shopify",
            "variant_title": "Green / 38",
            "vendor": "Urban Style",
            "tax_lines": [],
            "duties": [],
            "discount_allocations": []
          }
        ],
        "refunds": [],
        "shipping_address": {
          "first_name": "Ian",
          "address1": "88 riff road",
          "phone": null,
          "city": "New City",
          "zip": "10384",
          "province": "KwaZulu-Natal",
          "country": "South Africa",
          "last_name": "Owira",
          "address2": "Building 44",
          "company": null,
          "latitude": null,
          "longitude": null,
          "name": "Ian Owira",
          "country_code": "ZA",
          "province_code": "NL"
        },
        "shipping_lines": []
      },
      {
        "id": 3863991058502,
        "admin_graphql_api_id": "gid://shopify/Order/3863991058502",
        "app_id": 1354745,
        "browser_ip": null,
        "buyer_accepts_marketing": false,
        "cancel_reason": null,
        "cancelled_at": null,
        "cart_token": null,
        "checkout_id": 22230909124678,
        "checkout_token": "ecd7a0f6fc023ba2fc26ceff22e33dab",
        "client_details": {
          "accept_language": null,
          "browser_height": null,
          "browser_ip": null,
          "browser_width": null,
          "session_hash": null,
          "user_agent": null
        },
        "closed_at": null,
        "confirmed": true,
        "contact_email": "ian+customer@owira.co.za",
        "created_at": "2021-08-06T15:39:23+02:00",
        "currency": "ZAR",
        "current_subtotal_price": "179.99",
        "current_subtotal_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "current_total_discounts": "0.00",
        "current_total_discounts_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "current_total_duties_set": null,
        "current_total_price": "179.99",
        "current_total_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "current_total_tax": "0.00",
        "current_total_tax_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "customer_locale": "en",
        "device_id": null,
        "discount_codes": [],
        "email": "ian+customer@owira.co.za",
        "financial_status": "paid",
        "fulfillment_status": null,
        "gateway": "manual",
        "landing_site": null,
        "landing_site_ref": null,
        "location_id": null,
        "name": "2000156736",
        "note": null,
        "note_attributes": [],
        "number": 155736,
        "order_number": 156736,
        "order_status_url": "https://planet54.com/19759333/orders/60a22dd6a07105ae2b5db2c19cf6bb59/authenticate?key=f6f6ee8dd6b1c2a6b5a0f46822f18727",
        "original_total_duties_set": null,
        "payment_gateway_names": [
          "manual"
        ],
        "phone": "+27845741159",
        "presentment_currency": "ZAR",
        "processed_at": "2021-08-06T15:39:23+02:00",
        "processing_method": "manual",
        "reference": null,
        "referring_site": null,
        "source_identifier": null,
        "source_name": "shopify_draft_order",
        "source_url": null,
        "subtotal_price": "179.99",
        "subtotal_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "tags": "",
        "tax_lines": [],
        "taxes_included": false,
        "test": false,
        "token": "60a22dd6a07105ae2b5db2c19cf6bb59",
        "total_discounts": "0.00",
        "total_discounts_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "total_line_items_price": "179.99",
        "total_line_items_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "total_outstanding": "0.00",
        "total_price": "179.99",
        "total_price_set": {
          "shop_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "179.99",
            "currency_code": "ZAR"
          }
        },
        "total_price_usd": "12.40",
        "total_shipping_price_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "total_tax": "0.00",
        "total_tax_set": {
          "shop_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          },
          "presentment_money": {
            "amount": "0.00",
            "currency_code": "ZAR"
          }
        },
        "total_tip_received": "0.00",
        "total_weight": 500,
        "updated_at": "2021-08-06T15:39:27+02:00",
        "user_id": 117562257,
        "billing_address": {
          "first_name": "Ian",
          "address1": "88 riff road",
          "phone": null,
          "city": "New City",
          "zip": "10384",
          "province": "KwaZulu-Natal",
          "country": "South Africa",
          "last_name": "Owira",
          "address2": "Building 44",
          "company": null,
          "latitude": null,
          "longitude": null,
          "name": "Ian Owira",
          "country_code": "ZA",
          "province_code": "NL"
        },
        "customer": {
          "id": 5944958353,
          "email": "ian+customer@owira.co.za",
          "accepts_marketing": false,
          "created_at": "2017-07-31T22:07:26+02:00",
          "updated_at": "2021-08-06T15:39:37+02:00",
          "first_name": "Ian",
          "last_name": "Owira",
          "orders_count": 14,
          "state": "enabled",
          "total_spent": "2189.92",
          "last_order_id": 3863991058502,
          "note": "",
          "verified_email": true,
          "multipass_identifier": null,
          "tax_exempt": false,
          "phone": "+27845741159",
          "tags": "Black Friday signup, blackfriday2019, blackfridaysms1, cybermonday, Female, Galactic, gender_Female, more4, newsletter, paydaysms, Planetary, Super Nova, testsms",
          "last_order_name": "2000156736",
          "currency": "ZAR",
          "accepts_marketing_updated_at": "2020-10-31T12:28:37+02:00",
          "marketing_opt_in_level": null,
          "tax_exemptions": [],
          "admin_graphql_api_id": "gid://shopify/Customer/5944958353",
          "default_address": {
            "id": 6653918150726,
            "customer_id": 5944958353,
            "first_name": "Ian",
            "last_name": "Owira",
            "company": null,
            "address1": "88 riff road",
            "address2": "Building 44",
            "city": "New City",
            "province": "KwaZulu-Natal",
            "country": "South Africa",
            "zip": "10384",
            "phone": null,
            "name": "Ian Owira",
            "province_code": "NL",
            "country_code": "ZA",
            "country_name": "South Africa",
            "default": true
          }
        },
        "discount_applications": [],
        "fulfillments": [],
        "line_items": [
          {
            "id": 10039083630662,
            "admin_graphql_api_id": "gid://shopify/LineItem/10039083630662",
            "destination_location": {
              "id": 2945537638470,
              "country_code": "ZA",
              "province_code": "NL",
              "name": "Ian Owira",
              "address1": "88 riff road",
              "address2": "Building 44",
              "city": "New City",
              "zip": "10384"
            },
            "fulfillable_quantity": 1,
            "fulfillment_service": "manual",
            "fulfillment_status": null,
            "gift_card": false,
            "grams": 500,
            "name": "2 Piece Outfit - Green - Green / 38",
            "origin_location": {
              "id": 1403065991238,
              "country_code": "ZA",
              "province_code": "NL",
              "name": "Planet54",
              "address1": "42 Brickworks Way, Durban North",
              "address2": "",
              "city": "Durban",
              "zip": "4051"
            },
            "pre_tax_price": "179.99",
            "pre_tax_price_set": {
              "shop_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              },
              "presentment_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              }
            },
            "price": "179.99",
            "price_set": {
              "shop_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              },
              "presentment_money": {
                "amount": "179.99",
                "currency_code": "ZAR"
              }
            },
            "product_exists": true,
            "product_id": 4601084837958,
            "properties": [],
            "quantity": 1,
            "requires_shipping": true,
            "sku": "100000177121",
            "taxable": true,
            "title": "2 Piece Outfit - Green",
            "total_discount": "0.00",
            "total_discount_set": {
              "shop_money": {
                "amount": "0.00",
                "currency_code": "ZAR"
              },
              "presentment_money": {
                "amount": "0.00",
                "currency_code": "ZAR"
              }
            },
            "variant_id": 31918572306502,
            "variant_inventory_management": "shopify",
            "variant_title": "Green / 38",
            "vendor": "Urban Style",
            "tax_lines": [],
            "duties": [],
            "discount_allocations": []
          }
        ],
        "refunds": [],
        "shipping_address": {
          "first_name": "Ian",
          "address1": "88 riff road",
          "phone": null,
          "city": "New City",
          "zip": "10384",
          "province": "KwaZulu-Natal",
          "country": "South Africa",
          "last_name": "Owira",
          "address2": "Building 44",
          "company": null,
          "latitude": null,
          "longitude": null,
          "name": "Ian Owira",
          "country_code": "ZA",
          "province_code": "NL"
        },
        "shipping_lines": []
      }
    ];

    orders.forEach(order => {
      order.toggle_summary = false;
    })

    this.setData({ orders });
    /*
    const self = this;
    my.request({
      url: `${baseURL}/order/${id}`,
      method: 'GET',
      dataType: "json",
      success: function (res) {
        const orders = res.data;

        self.setData({ orders })

      },
      fail: function () { },
    });
    */
  },
  trackOrder(evt) {
    my.navigateTo({ url: `../tracking/tracking?id=${evt.target.targetDataset.value}` })
  },
  onToggleSummery(evt){
    const orders = this.data.orders;
    
    if (orders[evt.target.dataset.key].toggle_summary) {
      orders[evt.target.dataset.key].toggle_summary = false;
    } else {
      orders[evt.target.dataset.key].toggle_summary = true;
    }
    console.log(orders[evt.target.dataset.key].toggle_summary);
    
    this.setData({ orders });
  }
});
