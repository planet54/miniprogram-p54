const Shopify = require('../../util/Shopify');
const baseURL = 'https://p54-miniprogram.herokuapp.com';
Page({
  data: {
    products: [],
    accessToken: false,
    authCode: false,
    customer: false
  },
  onLoad() {
    this.accessToken();
  },
  onReady() {
    this.productListings();
  },
  productListings() {
    const self = this;

    const productsPromise = Shopify.getProductListings();

    productsPromise.then(res => {
  
      const { products } = res.data;
  
      self.setData( { products } );

    }).catch((err) => {
      console.error('product listings fetcherror', err);
    });
  },
  onUpdateProducts(items) {
    const products = items;

    this.setData({ products });
  },

  accessToken() {
    const self = this;
    let accessToken = self.data.accessToken;
    const app = getApp();
    let customer = app.globalData.customer;

    my.getAuthCode({
      scopes: ["auth_user"],
      success: (response) => {
        const authCode = response.authCode;
        self.setData({ authCode });
        my.request({
          url: `${baseURL}/auth`,
          method: 'POST',
          data: {
            authCode: authCode
          },
          dataType: "json",
          success: function (res) {
            const accessTokenResp = res.data;

            accessToken = accessTokenResp.accessToken;

            customer.id = accessTokenResp.customerId;

            self.setData({ customer, accessToken });

            self.customerAuth();

          },
          fail: function () {},
        });
      },
      fail: function () {}
    });
  },
  customerAuth() {
    const self = this;
    const app = getApp();
    let customer = app.globalData.customer;

    if (customer.details) return;

    my.getAuthCode({
      scopes: ["auth_user"],
      success: (response) => {
        const authCode = response.authCode;

        self.setData({ authCode })

        my.request({
          url: `${baseURL}/user`,
          method: 'POST',
          data: {
            authCode: authCode
          },
          dataType: "json",
          success: function (res) {
            const user = res.data;

            if (user.resultStatus === 'F' || user.errors) {
              my.alert({
                title: 'User auth error',
                content: JSON.stringify(user)
              });
              return;
            }

            if (user.customer) {
              customer.details = user.customer;

              self.setData({ customer });
             
              self.shopifyUserAuth();
            }

          },
          fail: function () {},
        });
      },
      fail: function () {}
    });
  },
  shopifyUserAuth() {
    const self = this;
    const app = getApp();
    let customer = app.globalData.customer;

    let url = new URL(`${baseURL}/user`);

    const params = {
      email: '',
      phone: ''
    }

    if (customer.details.userName.firstName) {
      params.first_name = customer.details.userName.firstName
    }

    if (customer.details.userName.lastName) {
      params.last_name = customer.details.userName.lastName
    }

    customer.details.contactInfos.forEach(info => {
      const type = info.contactType;
      switch (type.toLowerCase()) {
        case 'email':
          params.email = info.contactNo
          break;

        case 'mobile_phone':
          params.phone = info.contactNo
          break;

        default:
          break;
      }
    })

    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

    my.request({
      url: url,
      method: 'GET',
      data: {},
      dataType: "json",
      success: function (res) {
        const data = res.data;

        if (data.errors) {
          my.alert({
            title: 'Shop error',
            content: JSON.stringify(data.errors)
          });
          return;
        }

        app.globalData.customer.shopify = data;

        customer = app.globalData.customer;

        self.setData({ customer });

      },
      fail: function () {},
    });
  },
  methods: {},
});
