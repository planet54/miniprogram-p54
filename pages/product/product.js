const Cart = require('../../util/cart');

Page({
  data: {
    pageid: false,
    product: false,
    selectedVariant: false,
    cart: false,
    processing: false
  },
  methods: {},
  onLoad(params) {

    if (!params.id) {
      my.alert({
        title: 'Product',
        content: 'Cannot load product'
      });
      return;
    }

    const app = getApp();
    const globalData = app.globalData;

    const cart = globalData.cart;

    let pageid = params.id;

    this.loadProduct(pageid);

    this.setData({cart, pageid});

  },
  loadProduct(id) {

    if (!id) return;

    const Shopify = require('../../util/Shopify');
    const productResponse = Shopify.getProduct(id);

    productResponse.then((res) => {
      const product = res.data.product;
      this.setData({ product });
    });

  },
  onAddToCart() {

    if (!this.data.selectedVariant) {
      my.alert({
        title: 'Product',
        content: 'Select a size'
      });
      return;
    }
  
    const product_id = this.data.product.product_id;
    const variant = this.data.selectedVariant;
    let processing = true;

    this.setData({ processing });

    let cartAddRes = Cart.set({
      id: product_id,
      title: this.data.product.title,
      variant_id: variant.id,
      quantity: 1,
      price: parseInt(variant.price.replace('.', ''))
    });

    processing = false;

    const count = cartAddRes.item_count;

    this.setData({ count, processing });

  },
  onVariantClick(evt) {

    const variants = this.data.product.variants;
    let selectedVariant = this.data.selectedVariant;

    const found = variants.find(element => {
      return element.title.indexOf(evt.target.dataset.variantValue) > -1;
    });
    
    if (!found.available) {
      my.alert({
        title: 'Product',
        content: `Selected ${evt.target.dataset.variantValue} variant is unavailable`
      });
      return;
    }

    selectedVariant = found;

    this.setData({ selectedVariant })

  }
});
