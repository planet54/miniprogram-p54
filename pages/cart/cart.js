const Shopify = require('../../util/Shopify');
const Cart = require('../../util/cart');
const urlBase = 'https://p54-miniprogram.herokuapp.com';

Page({
  data: {
    products: [],
    line_items: [],
    item_count: 0,
    total_price: 0,
    formatted_total_price: 'R 0',
    cartObject: false,
    errorMesseges: false,
    successMessages: false,
    warningMessages: false,
    processing: false,

    // USER STUFF
    customer: null,
    accessToken: null,
    authCode: null,
    info: false
  },
  didMount() { },
  didUpdate() { },
  onLoad() {
    this.loadCart();
    this.accessToken();
  },
  loadCart() {

    const storedCart = Cart.get();

    if (!storedCart) return;
    const app = getApp();
    const self = this;
    const cartItems = storedCart.items;

    app.globalData.cart = storedCart;

    let products = this.data.products;
    let item_count = this.data.item_count;
    let total_price = this.data.total_price;
    let line_items = this.data.line_items;
    let formatted_total_price = this.data.formatted_total_price;

    total_price = storedCart.total_price;
    formatted_total_price = this.formatMoney(total_price);
    item_count = storedCart.item_count;
    line_items = storedCart.items;

    cartItems.forEach((product) => {

      const productPromise = Shopify.getProduct(product.id);

      productPromise.then((res) => {

        const retrievedProduct = res.data.product;

        retrievedProduct.variants.forEach((variant) => {

          if (variant.id === product.variant_id) {
            retrievedProduct.variant = variant;

            let price = parseInt(variant.price.replace('.', ''));

            retrievedProduct.variant.quantity = product.quantity;
            retrievedProduct.variant.price_formatted = self.formatMoney(price);
          }
        });

        products.push(retrievedProduct);

        this.setData({ products });

      }).catch((err) => console.error('product fetching error', err))

    });

    this.setData({
      products,
      line_items,
      item_count,
      total_price,
      formatted_total_price
    });

    console.log(this.data);

  },
  onContinueShopping() {
    my.navigateTo({ url: "../index/index" });
  },
  formatMoney(cents, format) {
    const moneyFormat = 'R{{amount}}';

    if (typeof cents === 'string') {
      cents = cents.replace('.', '');
    }

    let value = '';
    const placeholderRegex = /\{\{\s*(\w+)\s*\}\}/;
    const formatString = format || moneyFormat;

    function formatWithDelimiters(
      number,
      precision = 2,
      thousands = ',',
      decimal = '.'
    ) {
      if (isNaN(number) || number == null) {
        return 0;
      }

      number = (number / 100.0).toFixed(precision);

      const parts = number.split('.');
      const dollarsAmount = parts[0].replace(
        /(\d)(?=(\d\d\d)+(?!\d))/g,
        `$1${thousands}`
      );
      const centsAmount = parts[1] ? decimal + parts[1] : '';

      return dollarsAmount + centsAmount;
    }

    switch (formatString.match(placeholderRegex)[1]) {
      case 'amount':
        value = formatWithDelimiters(cents, 2);
        break;
      case 'amount_no_decimals':
        value = formatWithDelimiters(cents, 0);
        break;
      case 'amount_with_comma_separator':
        value = formatWithDelimiters(cents, 2, '.', ',');
        break;
      case 'amount_no_decimals_with_comma_separator':
        value = formatWithDelimiters(cents, 0, '.', ',');
        break;
    }

    return formatString.replace(placeholderRegex, value);
  },
  formatPrice(string) {
    return parseInt(string.replace('.', ''));
  },
  qtySubtract(evt) {
    const app = getApp();
    const globalData = app.globalData;
    const id = evt.target.dataset.key;
    let products = this.data.products;
    let line_items = this.data.line_items;

    const line_items_index = line_items.findIndex((item) => item.variant_id === id);

    if (line_items_index === -1) return;

    let line_item = line_items[line_items_index];

    const product_index = products.findIndex((product) => product.product_id === line_item.id);

    if (product_index === -1) return;

    let product = products[product_index];
    
    if (line_item.quantity === 0 && product.variant.quantity === 0) return;
    
    line_item.quantity = line_item.quantity - 1;
    product.variant.quantity = product.variant.quantity - 1;

    globalData.cart.items = line_items;

    this.setData({ products, line_items });

    this.updateCart();

  },
  qtyAdd(evt) {
    const app = getApp();
    const globalData = app.globalData;
    const id = evt.target.dataset.key;
    let products = this.data.products;
    let line_items = this.data.line_items;

    const line_items_index = line_items.findIndex((item) => item.variant_id === id);

    if (line_items_index === -1) return;

    let line_item = line_items[line_items_index];

    const product_index = products.findIndex((product) => product.product_id === line_item.id);

    if (product_index === -1) return;

    let product = products[product_index];

    if (line_item.quantity < product.variant.inventory_quantity) {
      line_item.quantity = line_item.quantity + 1;
      product.variant.quantity = product.variant.quantity + 1;
    } else {
      my.alert({
        title: 'Cart',
        content: 'Maximum quantity'
      })
    }

    globalData.cart.items = line_items;

    this.setData({ products, line_items });

    this.updateCart();

  },
  removeLineItem(evt) {

    const app = getApp();
    const globalData = app.globalData;
    const id = evt.target.dataset.key;
    let products = this.data.products;
    let line_items = this.data.line_items;

    products = products.filter(product => product.variant.id !== id);

    line_items = line_items.filter(item => item.variant_id !== id);

    globalData.cart.items = line_items;

    this.setData({ products, line_items });

    this.updateCart();

  },
  updateCart() {
    const app = getApp();
    const globalData = app.globalData;
    let total_price = 0
    const item_count = globalData.cart.items.length

    globalData.cart.items.forEach(item => {
      const totalItemPrice = item.price * item.quantity;
      total_price = total_price + totalItemPrice
    })

    globalData.cart.item_count = item_count;
    globalData.cart.total_price = total_price;

    const formatted_total_price = this.formatMoney(total_price);

    this.setData({ total_price, item_count, formatted_total_price });

  },
  onCheckout() {
    this.checkout();
  },
  accessToken() {
    const self = this;
    let accessToken = self.data.accessToken;
    const app = getApp();
    let customer = app.globalData.customer;

    my.getAuthCode({
      scopes: ["auth_user"],
      success: (response) => {
        const authCode = response.authCode;
        self.setData({ authCode });
        my.request({
          url: 'https://p54-miniprogram.herokuapp.com/auth',
          method: 'POST',
          data: {
            authCode: authCode
          },
          dataType: "json",
          success: function (res) {
            const accessTokenResp = res.data;

            accessToken = accessTokenResp.accessToken;

            customer.id = accessTokenResp.customerId;

            self.setData({ customer, accessToken });


          },
          fail: function (res) {
            const error = res.data.error;

            my.alert({
              title: 'Access token error',
              content: 'Failed to fetch access token ' + JSON.stringify(error)
            });
          },
        });
      },
      fail: function () {
        my.alert({
          title: 'AuthCode error',
          content: 'Failed to fetch Authcode'
        });
      }
    });
  },
  checkout() {
    const self = this;
    const app = getApp();
    const globalData = app.globalData;
    const customer = this.data.customer;
    const accessToken = this.data.accessToken;

    if (!customer) return;

    let processing = true;

    this.setData({ processing })

    if (globalData.customer.shopify && !globalData.customer.shopify.addresses.length) {
      my.navigateTo({ url: `../address/address?id=${globalData.customer.shopify.id}` });
      return;
    }

    if (globalData.cart.total_price < 45000) {
      globalData.cart.total_price = globalData.cart.total_price + 7500;
    }

    my.getAuthCode({
      scopes: ["auth_user"],
      success: (response) => {
        const authCode = response.authCode;

        self.setData({ authCode });

        my.request({
          url: `${urlBase}/pay`,
          method: 'POST',
          data: {
            authCode: authCode,
            accessToken: accessToken,
            customerId: customer.id,
            data: globalData.cart
          },
          dataType: "json",
          success: function (res) {
            const data = res.data;

            my.tradePay({
              paymentUrl: data.redirectActionForm.redirectUrl,
              success: (res) => {
                const code = res.resultCode;
                let successMessages = self.data.successMessages;
                let warningMessages = self.data.warningMessages;
                let errorMesseges = self.data.errorMesseges;

                switch (code) {
                  case '9000':
                    successMessages = 'Successful Payment';
                    self.setData({ successMessages });
                    self.createOrder();
                    break;

                  case '8000':
                    warningMessages = 'Payment is processing';
                    self.setData({ warningMessages });
                    break;

                  case '4000':
                    errorMesseges = 'Payment failed';
                    self.setData({ errorMesseges });
                    break;

                  case '6001':
                    warningMessages = 'Payment process canceled';
                    self.setData({ warningMessages });
                    break;

                  default:
                    break;
                }

                const processing = false;

                self.setData({ successMessages, processing });

              },
              fail: (res) => {
                const processing = false;

                self.setData({ successMessages, processing });

                my.alert({
                  title: "Payment Error",
                  content: JSON.stringify(res),
                });
              }
            });

          },
          fail: function (res) {
            const error = res.data.error;

            my.alert({
              title: 'Payment error',
              content: JSON.stringify(error)
            });
          },
        });
      },
      fail: function () {
        my.alert({
          title: 'AuthCode error',
          content: 'Failed to fetch Authcode'
        });
      }
    });

  },
  createOrder() {
    const self = this;
    const app = getApp();
    let globalData = app.globalData;
    let line_items = []

    globalData.cart.items.forEach(item => {
      line_items.push({
        variant_id: item.variant_id,
        quantity: item.quantity,
      })
    })

    my.request({
      url: 'https://p54-miniprogram.herokuapp.com/order/create/',
      method: 'POST',
      data: {
        "email": globalData.customer.shopify.email,
        "shipping_address": app.globalData.customer.shopify.addresses[0],
        "send_receipt": true,
        line_items: line_items
      },
      dataType: "json",
      success: function (res) {
        let successMessages = 'Order placed!';
        self.setData({ successMessages });
        if (res.data.order) {
          my.navigateTo({ url: `../thank-you/thank-you?id=${res.data.order.id}` });
        }
      },
      fail: function () { },
    });

  }
});
