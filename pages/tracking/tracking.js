Page({
  data: {
    trackingLink: false
  },
  onLoad(params) {
    if (params.id) {
      let trackingLink = `https://planet54.justcop.it/r/${params.id}` 
      this.setData({ trackingLink })
    }
  },
});
