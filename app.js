const Cart = require('./util/cart');
App({
  globalData: {
    cart: {
      total_price: 0,
      item_count: 0,
      items: []
    },
    pagination: {
      prev: false,
      next: false
    },
    customer: {
      id: null,
      details: null,
      shopify: null
    }
  },

  onLaunch(options) {
    // 第一次打开
    // options.query == {number:1}
    my.clearStorage()
  },
  onShow(options) {
    // 从后台被 scheme 重新打开
    // options.query == {number:1}
  },
});
