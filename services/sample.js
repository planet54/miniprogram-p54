Page({

  data: {

    customerId: null,

    authCode: null,

    accessToken: null,

    clientId: "2020122653946739963336" //Sandbox

  },



  // data: {

  //   customerId: "216610000000302952883",

  //   authCode: null,

  //   accessToken: null,

  //   clientId: "2020122653946739963336"

  // },

  onLaunch() {

    var app = getApp()

    this.setData({

      customerId: app.globalData.customerId

    })

  },



  onAuthAndToken() {

    console.log('onAuth')

    const self = this;



    // 1. get AuthCode (Mini-Program/FE)

    my.getAuthCode({

      scopes: ["auth_user"],

      success: (res) => {

        // 2. on Successful AuthCode get AccessToken (Merchant/BE)

        const self = this;

        self.setData({ authCode: res.authCode })



        my.request({

          url:

            "https://vodapay-gateway.sandbox.vfs.africa/v2/authorizations/applyToken",

          method: "POST",

          headers: {

            "client-id": self.data.clientId,

            signature:

              "algorithm=RSA256, keyVersion=1, signature=testing_signatur",

            "request-time": "2021-02-22T17:49:26.913+08:00",

          },

          data: {

            grantType: "AUTHORIZATION_CODE",

            authCode: res.authCode,

          },

          dataType: "json",

          success: function (res) {

            my.alert({ content: "customerID RESPONSE: " + res.data.customerId });

            var app = getApp()

            // app.globalData.customerId = res.data.customerId;

            self.setData({ customerId: res.data.customerId })

            my.alert({ content: "AccessToken RESPONSE: " + res.data.accessToken });

            self.setData({ accessToken: res.data.accessToken })



          },

          fail: function (res) {

            const json = JSON.stringify(res);

            my.alert({ content: "AccessToken FAILED: " + json });



          },

        });

      },

      fail: function (res) {

        const json = JSON.stringify(res);

        my.alert({ content: "getAuthCode FAILED" + json });

      },

    });

  },



  onTradePay() {

    my.tradePay({

      paymentUrl:

        "http://vodapay-m.sandbox.vfs.africa/m/portal/cashier/checkout?bizNo=20210222111212800100166353500084689&timestamp=1619158518246&mid=216620000000179991384&sign=jNj7Q8yELWuGCCYCEvyBH19pepPcex%2Fj%2BDyXGvQ0%2BOobV3HWAOTRpkNYzBpDF4Q%2BYVQPIGIKj55llP%2BDHJNfYMFj%2FZX261txjrkMRuLGEexdtvLj2fwgkstr6f96BfaupHuu3LPvRRXtmTDpGZ%2FIvr%2B5kVmhIHXLRlaTi3JCCkk4xusvtYIe0F134JdsOParju5c93ZkwL9lm%2FEZUCWCdWtfVD0XkVxhnEfyo5VdldjEheiP4Nz24qnrRB7iuIkTk4zTYbw1Mp%2BsHVOpqs6yVZ%2Fx3BuSuO648SrGpBFF88W6WINzn%2F2Qv0pOMHXp9SlKPYIQrZroIcrSsCoiTSuvNg%3D%3D",

      success: function (res) {

        const json = JSON.stringify(res);

        my.alert({ content: "TradePay RESPONSE: " + json });

      },

      fail: function (res) {

        const json = JSON.stringify(res);

        my.alert({ content: "TradePay FAILED: " + json });

      },

    });

  },

  onPay() {

    console.log('onPay')

    const self = this;



    const selfMy = my;



    const paymentRequestId = "c0a83b1716139873717931001" + Math.floor(Math.random() * 1000);



    console.log('new payment request ID:', paymentRequestId)



    // 1. create Payment Identity  (BE)

    my.request({

      url: "https://vodapay-gateway.sandbox.vfs.africa/v2/payments/pay",

      method: "POST",

      headers: {

        "client-id": self.data.clientId,

        signature: "algorithm=RSA256, keyVersion=1, signature=testing_signatur",

        "request-time": "2021-02-22T17:49:26.913+08:00",

      },

      data: {

        productCode: "CASHIER_PAYMENT",

        salesCode: "51051000101000000011",

        // salesCode: salesCode,

        paymentNotifyUrl:

          "http://mock.vision.vodacom.aws.corp/mock/api/v1/payments/notifyPayment.htm",

        // paymentRequestId: "c0a83b17161398737179310015310",

        paymentRequestId: paymentRequestId,

        paymentRedirectUrl:

          "http://mock.vision.vodacom.aws.corp/mock/api/v1/payments/notifyPayment.htm",

        paymentExpiryTime: "2022-02-22T17:49:31+08:00",

        paymentAmount: {

          currency: "ZAR",

          value: "6234",

        },

        order: {

          goods: {

            referenceGoodsId: "goods123",

            goodsUnitAmount: {

              currency: "ZAR",

              value: "2000",

            },

            goodsName: "mobile1",

          },

          env: {

            terminalType: "MINI_APP",

          },

          orderDescription: "title",

          buyer: {

            referenceBuyerId: this.data.customerId,

          },

        },

      },

      dataType: "json",

      success: function (res) {

        const json = JSON.stringify(res);

        // const json = JSON.stringify(res.data);

        my.alert({ content: "Payment Identity RESPONSE: " + json });



        // 2. Open Cashier Page and Transact (FE)

        // my.alert({ content: "Payment Identity RESPONSE: " + json });



        self.tradePayViaUrl(res.data.redirectActionForm.redirectUrl)

        // my.tradePay({

        //   tradeNO: res.data.paymentId,

        //   success: function (res) {

        //     const json = JSON.stringify(res);

        //     my.alert({ content: "TradePay RESPONSE: " + json });

        //   },

        //   fail: function (res) {

        //     const json = JSON.stringify(res);

        //     my.alert({ content: "TradePay FAILED: " + json });

        //   },

        // });

      },

      fail: function (res) {

        const json = JSON.stringify(res);

        my.alert({ content: "Payment Identity FAILED: " + json });

      },

    });

  },

  tradePayViaUrl(url) {

    console.log('tradePay url', url)

    my.alert({ content: "opening my.tradePay: " + url });

    my.tradePay({

      paymentUrl: url,

      success: function (res) {

        const json = JSON.stringify(res);

        my.alert({ content: "TradePay RESPONSE: " + json }); //90000

      },

      fail: function (res) {

        const json = JSON.stringify(res);

        my.alert({ content: "TradePay FAILED: " + json });

      },

    });

  },

  onTradePayReactive() {

    this.tradePayReactive();

  },

  tradePayReactive() {

    my.tradePay({

      paymentUrl:

        "http://vodapay-m.sandbox.vfs.africa/m/portal/cashier/checkout?bizNo=20210222111212800100166353500084689&timestamp=1619158518246&mid=216620000000179991384&sign=jNj7Q8yELWuGCCYCEvyBH19pepPcex%2Fj%2BDyXGvQ0%2BOobV3HWAOTRpkNYzBpDF4Q%2BYVQPIGIKj55llP%2BDHJNfYMFj%2FZX261txjrkMRuLGEexdtvLj2fwgkstr6f96BfaupHuu3LPvRRXtmTDpGZ%2FIvr%2B5kVmhIHXLRlaTi3JCCkk4xusvtYIe0F134JdsOParju5c93ZkwL9lm%2FEZUCWCdWtfVD0XkVxhnEfyo5VdldjEheiP4Nz24qnrRB7iuIkTk4zTYbw1Mp%2BsHVOpqs6yVZ%2Fx3BuSuO648SrGpBFF88W6WINzn%2F2Qv0pOMHXp9SlKPYIQrZroIcrSsCoiTSuvNg%3D%3D",

      success: function (res) {

        const json = JSON.stringify(res);

        my.alert({ content: "TradePay RESPONSE: " + json });

      },

      fail: function (res) {

        const json = JSON.stringify(res);

        my.alert({ content: "TradePay FAILED: " + json });

      },

    });

  },

});